package com.android.yulia.poledance.net;

import android.util.Log;

import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.net.model.ElementModel;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thenameless on 11.01.16.
 */
public class ParseHelper {

    public static void getElements(final GenericFindCallback<ElementModel> callback) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PoleTrick");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    callback.find(fromParse(objects));
                } else {
                    callback.error(e);
                }

            }
        });

    }

    private static ArrayList<ElementModel> fromParse(List<ParseObject> objects) {

        final ArrayList<ElementModel> elementModels = new ArrayList<>(objects.size());
        for (ParseObject parseObject : objects) {
            final ElementModel elementModel = new ElementModel(parseObject);
            elementModels.add(elementModel);
        }
        return  elementModels;
    }

    public static void login(String name, String password, LogInCallback  callback) {
        ParseUser.logInInBackground(name, password, callback);
    }

    public static void logout(){
        ParseUser.logOut();
    }

    public static void signUp(String name, String password, SignUpCallback signUpCallback) {
        ParseUser user = new ParseUser();
        user.setUsername(name);
        user.setPassword(password);
//        user.setEmail("email@example.com");

        user.signUpInBackground(signUpCallback);
    }

    public static void setFavorite(String elementId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PoleTrick");

        query.getInBackground(elementId, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    List<String> favoritesList = object.getList("favorites");
                    if (favoritesList == null) {
                        favoritesList = new ArrayList<String>();
                    }

                    String userId = ParseUser.getCurrentUser().getObjectId();
                    if (favoritesList.contains(userId)) {
                        favoritesList.remove(userId);
                    } else {
                        favoritesList.add(userId);
                    }
                    object.put("favorites", favoritesList);
                    object.saveInBackground();
                }
            }
        });
    }

    public static void setDone(String elementId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PoleTrick");

        query.getInBackground(elementId, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    List<String> doneList = object.getList("done");
                    if (doneList == null) {
                        doneList = new ArrayList<String>();
                    }

                    String userId = ParseUser.getCurrentUser().getObjectId();
                    if (doneList.contains(userId)) {
                        doneList.remove(userId);
                    } else {
                        doneList.add(userId);
                    }
                    object.put("done", doneList);
                    object.saveInBackground();
                }
            }
        });
    }

    public static void createElement(String name, String description,
                                     ArrayList<PhotoDbModel> photos, boolean favorite, boolean done, final SaveElementCallback callback) {

        final ParseObject element = new ParseObject("PoleTrick");
        element.put("name", name);
        element.put("description", description);

        ArrayList<String> urls = new ArrayList<>(photos.size());
        for (PhotoDbModel photoDbModel: photos) {
            urls.add(photoDbModel.getUrl());
        }
        element.put("photos", urls);
        if (favorite) {
            element.put("favorites", new String[]{ParseUser.getCurrentUser().getObjectId()});
        }
        if (done) {
            element.put("done", new String[]{ParseUser.getCurrentUser().getObjectId()});
        }
        element.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    try {
                        DatabaseManager.insertOrUpdateModel(new ElementModel(element));
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }

                    callback.onSaved(element.getObjectId());
                } else {
                    callback.error(e.getLocalizedMessage());
                }
            }
        });
//        element.saveEventually();
    }
}
