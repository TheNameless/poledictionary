package com.android.yulia.poledance.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.ui.activity.CreateElementActivity;
import com.android.yulia.poledance.ui.activity.ElementViewActivity;
import com.android.yulia.poledance.ui.adapter.ElementsAdapter;
import com.android.yulia.poledance.ui.views.ElementClickListener;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by user on 16-01-16.
 */
@EFragment
public abstract class BaseElementsFragment extends BaseFragment implements ElementClickListener, MaterialSearchView.SearchViewListener, MaterialSearchView.OnQueryTextListener {

    private ArrayList<PoleDbModel> poleDbModels = new ArrayList<>();
    protected ArrayList<PoleDbModel> searchModels = new ArrayList<>();
    protected ElementsAdapter adapter;
    private MaterialSearchView searchView;
    private static final int CREATE_ELEMENT_REQUEST_CODE = 10000;

    @ViewById(R.id.rvElementsList) RecyclerView rvElements;

    @Override
    public void onResume() {
        super.onResume();
        activityBridge.getBus().register(this);

    }

    @Click(R.id.fab)
    protected void newElementClicked() {
        CreateElementActivity.start(BaseElementsFragment.this, CREATE_ELEMENT_REQUEST_CODE);
    }

    @AfterViews
    public void onViewCreated() {
        rvElements.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchView = activityBridge.getSearchView();

        adapter = new ElementsAdapter(searchModels, this, rvElements);

        rvElements.setAdapter(adapter);
        rvElements.setItemAnimator(new DefaultItemAnimator());
        fillList();

        searchView.clearFocus();
        searchView.closeSearch();

        searchView.setOnSearchViewListener(this);
        searchView.setOnQueryTextListener(this);

        adapter.setEnableDragging(enableDragging());
    }

    protected abstract boolean enableDragging();

    @Override
    public void onSearchViewShown() {
        adapter.setEnableDragging(false);
    }

    @Override
    public void onSearchViewClosed() {
        adapter.setEnableDragging(true);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        search(newText);
        return false;
    }

    private void search(String newText) {
        for (PoleDbModel model : poleDbModels) {
            boolean containsQuery =  model.getName().toLowerCase().contains(newText.toLowerCase());

            if (!containsQuery && searchModels.contains(model)) {
                int index = searchModels.indexOf(model);
                searchModels.remove(index);

            } else if (!searchModels.contains(model) && containsQuery) {
                searchModels.add(model);
                Collections.sort(searchModels, comparator);
                adapter.notifyItemInserted(searchModels.indexOf(model));
            }
        }
        adapter.notifyDataSetChanged();

    }

    protected void fillList() {
        searchView.clearFocus();
        searchView.closeSearch();

        int size = poleDbModels.size();
        poleDbModels.clear();
        adapter.notifyItemRangeRemoved(0, size);

        poleDbModels.addAll(getElements());

        searchModels.clear();
        searchModels.addAll(poleDbModels);
        adapter.notifyItemRangeInserted(0, searchModels.size());
    };

    private Comparator<PoleDbModel> comparator = new Comparator<PoleDbModel>() {
        @Override
        public int compare(PoleDbModel s1, PoleDbModel s2) {
            return s2.getId() - s1.getId();
        }
    };

    protected abstract ArrayList<PoleDbModel> getElements();

    @Override
    public void onClick(int position, ImageView imageView) {
        ElementViewActivity.start(this, poleDbModels.get(position).getParseId(), position, imageView);
    }

    @Override
    public void onPause() {
        super.onPause();
        activityBridge.getBus().unregister(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_ELEMENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getNewElement(data);
            }
            return;
        }
        DatabaseManager.refreshModel(poleDbModels.get(requestCode));
        adapter.notifyItemChanged(requestCode);
    }

    private void getNewElement(Intent data) {
        String parseId = data.getStringExtra(CreateElementActivity.PARSE_ID_KEY);
        PoleDbModel model = DatabaseManager.getElementByParseId(parseId);
        if (!poleDbModels.contains(model)) {
            poleDbModels.add(0, model);
        }
        if (!searchModels.contains(model)) {
            searchModels.add(0, model);
        }
        adapter.notifyItemInserted(0);
        rvElements.scrollToPosition(0);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = activityBridge.getSearchView();
        searchView.setMenuItem(item);
    }

    protected void removeModel(int position) {
        PoleDbModel model = searchModels.get(position);

        searchModels.remove(model);
        poleDbModels.remove(model);
        adapter.notifyItemRemoved(position);
    }

    @Override
    public void onStop() {
        super.onStop();

        for (PoleDbModel model: searchModels) {
            DatabaseManager.updateModel(model);
        }
    }

}
