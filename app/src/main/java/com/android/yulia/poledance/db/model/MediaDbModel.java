package com.android.yulia.poledance.db.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by thenameless on 11.01.16.
 */

public class MediaDbModel {

    public final static String ELEMENT_URL_FIELD = "url";

    @DatabaseField(generatedId = true)
    private int Id;

    @DatabaseField(dataType = DataType.STRING, columnName = ELEMENT_URL_FIELD)
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private PoleDbModel poleDbModel;

    public PoleDbModel getPoleDbModel() {
        return poleDbModel;
    }

    public void setPoleDbModel(PoleDbModel poleDbModel) {
        this.poleDbModel = poleDbModel;
    }

    public MediaDbModel(){

    }
}
