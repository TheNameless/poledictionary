package com.android.yulia.poledance.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.net.ParseHelper;
import com.android.yulia.poledance.ui.activity.LoginActivity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

/**
 * Created by user on 29-01-16.
 */

@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    @Click(R.id.btn_logout)
    public void onClick(View v) {
        ParseHelper.logout();
        LoginActivity.start(getActivity());
        getActivity().finish();
    }
}
