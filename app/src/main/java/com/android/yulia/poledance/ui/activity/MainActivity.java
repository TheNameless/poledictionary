package com.android.yulia.poledance.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.yulia.poledance.net.model.ElementModel;
import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.utils.Utils;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.parse.ParseException;

import com.android.yulia.poledance.net.GenericFindCallback;
import com.android.yulia.poledance.net.ParseHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @ViewById(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @ViewById(R.id.search_view) MaterialSearchView searchView;
    @ViewById(R.id.srlElements)
    SwipeRefreshLayout refreshLayout;

    private ActionBarDrawerToggle mDrawerToggle;
    private MenuItem refreshItem;

    private static final String REFRESH_KEY = "refresh_key";

    public static void start(Context context) {
        start(context, false);
    }

    public static void start(Context context, boolean needRefresh) {
        MainActivity_.intent(context).extra(REFRESH_KEY, needRefresh).start();
    }

    @AfterViews
    protected void inflate() {

        setHomeIndicator();
        getFragmentLauncher().launchDrawerFragment();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        View mSearchLayout = searchView.findViewById(com.miguelcatalan.materialsearchview.R.id.search_layout);
        mSearchLayout.setVisibility(View.GONE);
        refreshLayout.setColorSchemeResources(R.color.colorAccent);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBus().register(this);
    }

    private void updateTricks() {

        animateRefresh();

        ParseHelper.getElements(new GenericFindCallback<ElementModel>() {
            @Override
            public void find(ArrayList<ElementModel> models) {
                try {
                    DatabaseManager.updateDB(models);

                    getBus().post(Utils.RESULT_OK);
                    completeRefresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                    completeRefresh();
                }
                refreshLayout.setRefreshing(false);

            }

            @Override
            public void error(ParseException e) {
                completeRefresh();
            }
        });

    }

    public void completeRefresh() {
        refreshItem.getActionView().clearAnimation();
        refreshItem.setActionView(null);
        refreshItem.setEnabled(true);
        refreshLayout.setRefreshing(false);
    }

    private void animateRefresh() {
        ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.view_refresh, null);

        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.ic_refresh);
        iv.startAnimation(rotation);

        refreshItem.setActionView(iv);
        refreshItem.setEnabled(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        refreshItem = menu.findItem(R.id.action_refresh);

        if (getIntent().getBooleanExtra(REFRESH_KEY, false)
                && DatabaseManager.getAllElements().size() == 0) {
            updateTricks();
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_refresh:
                updateTricks();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCloseDrawer() {
        mDrawerLayout.closeDrawers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getBus().unregister(this);
    }

    @Override
    public void onBackPressed() {

        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
            return;
        }

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public MaterialSearchView getSearchView() {
        return searchView;
    }

    @Override
    public void onRefresh() {
        updateTricks();
    }
}
