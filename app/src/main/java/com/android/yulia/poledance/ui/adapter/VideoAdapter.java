package com.android.yulia.poledance.ui.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.android.yulia.poledance.db.model.VideoDbModel;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import wseemann.media.FFmpegMediaMetadataRetriever;

/**
 * Created by thenameless on 16.01.16.
 */
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.Holder> {

    private List<VideoDbModel> models;
    private VideoListener listener;
    private int photoSize;

    public VideoAdapter(Activity context, List<VideoDbModel> objects, VideoListener photoListener) {
        models = objects;
        listener = photoListener;
        photoSize = (int) context.getResources().getDimension(R.dimen.photo_size_large);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        FFmpegMediaMetadataRetriever metadataRetriever = new FFmpegMediaMetadataRetriever();
//        metadataRetriever.setDataSource("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4", new HashMap<String, String>());
//        Bitmap b = metadataRetriever.getFrameAtTime(200, FFmpegMediaMetadataRetriever.OPTION_CLOSEST);
//        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(models.get(position).getUrl(),
//                MediaStore.Images.Thumbnails.MINI_KIND);

//        holder.image.setImageBitmap(b);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;

        public Holder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.ivPhoto);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onVideoClicked(models.get(getAdapterPosition()).getUrl());
                }
            });
        }
    }

    public interface VideoListener {
        void onVideoClicked(String url);
    }
}
