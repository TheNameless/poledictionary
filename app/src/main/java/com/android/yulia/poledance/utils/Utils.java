package com.android.yulia.poledance.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

/**
 * Created by thenameless on 16.01.16.
 */
public class Utils {

    public static final String SHARED_ELEMENT_NAME = "hero";
    public static final String RESULT_OK = "200";
    public static final String DEFAULT_PHOTO = "http://www.pilonomania.ru/img/40.jpg";

    public static void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Bundle getTransitionsBundle(Activity activity, ImageView imageView) {
        return ActivityOptionsCompat.makeSceneTransitionAnimation(
                activity,
                imageView,
                SHARED_ELEMENT_NAME).toBundle();
    }
}
