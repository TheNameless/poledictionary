package com.android.yulia.poledance.ui.adapter;

import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.ui.views.ElementClickListener;
import com.android.yulia.poledance.ui.views.ElementView;
import com.android.yulia.poledance.ui.views.ElementView_;
import com.makeramen.dragsortadapter.DragSortAdapter;
import com.makeramen.dragsortadapter.NoForegroundShadowBuilder;

import java.util.ArrayList;

/**
 * Created by thenameless on 12.01.16.
 */
public class ElementsAdapter extends DragSortAdapter<ElementsAdapter.Holder> {

    private ArrayList<PoleDbModel> models;
    private ElementClickListener listener;
    private boolean enableDragging = true;

    public void setEnableDragging(boolean enableDragging) {
        this.enableDragging = enableDragging;
    }

    public boolean isEnableDragging() {
        return enableDragging;
    }

    public ElementsAdapter(ArrayList<PoleDbModel> models, ElementClickListener listener, RecyclerView recyclerView) {
        super(recyclerView);
        this.models = models;
        this.listener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(ElementView_.build(parent.getContext(), listener));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final PoleDbModel poleDbModel = models.get(position);
        ElementView  elementView = (ElementView) holder.itemView;
        elementView.bind(poleDbModel, position);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public int getPositionForId(long id) {
        for (PoleDbModel model : models) {
            if (model.getId() == id) {
                return models.indexOf(model);
            }
        }
        return 0;
    }

    @Override
    public boolean move(int fromPosition, int toPosition) {
        models.add(toPosition, models.remove(fromPosition));
//        models.get(toPosition).setPosition(toPosition);
        return true;
    }

    @Override
    public long getItemId(int position) {
        return models.get(position).getId();
    }

    class Holder extends DragSortAdapter.ViewHolder {

        public Holder(View itemView) {
            super(ElementsAdapter.this, itemView);
            ElementView elementView = (ElementView) itemView;
            elementView.setDragListener(new DragListener() {
                @Override
                public void onDragStarted() {
                    if (enableDragging) {
                        startDrag();
                    }
                }
            });
        }

        @Override public View.DragShadowBuilder getShadowBuilder(View itemView, Point touchPoint) {
            return new NoForegroundShadowBuilder(itemView, touchPoint);
        }
    }

    public interface DragListener {
        void onDragStarted();
    }
}
