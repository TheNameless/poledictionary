package com.android.yulia.poledance.core;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.otto.Bus;

public interface ActivityBridge {

    FragmentLauncher getFragmentLauncher();

    void onCloseDrawer();

    void setHomeIndicator();

    void setActionbarTitle(int res);

    void setActionbarTitle(String title);

    Bus getBus();

    MaterialSearchView getSearchView();

}
