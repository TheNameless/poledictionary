package com.android.yulia.poledance.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.ui.adapter.PhotosAdapter;
import com.android.yulia.poledance.ui.views.SwipeListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import java.util.ArrayList;

/**
 * Created by user on 18-01-16.
 */

@EActivity(R.layout.activity_photos)
public class PhotosActivity extends Activity implements SwipeListener.SwipeCompleteListener{

    private static final String ID_KEY = "id_key";
    private static final String POSITION_KEY = "position_key";

    public static void start(Context context, String id, int position) {
        PhotosActivity_.intent(context).extra(ID_KEY, id).extra(POSITION_KEY, position).start();
    }

    @AfterViews
    protected void onCreate() {

        String id = getIntent().getStringExtra(ID_KEY);
        int position = getIntent().getIntExtra(POSITION_KEY, 0);

        PoleDbModel model = DatabaseManager.getElementByParseId(id);

        View base = findViewById(R.id.photoLayout);

        final SwipeListener swipeListener = new SwipeListener(this, base);
        PhotosAdapter photosAdapter = new PhotosAdapter(new ArrayList<>(model.getPhotos()), swipeListener);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        viewPager.setAdapter(photosAdapter);

        viewPager.setCurrentItem(position);
//
//        viewPager.setOnTouchListener(swipeListener);

    }


    @Override
    public void onSwipeComplete() {
        finish();
    }
}
