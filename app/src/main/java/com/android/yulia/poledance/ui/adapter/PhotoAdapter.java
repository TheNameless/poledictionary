package com.android.yulia.poledance.ui.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by thenameless on 16.01.16.
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.Holder> {

    private List<PhotoDbModel> models;
    private PhotoListener listener;
    private int photoSize;

    public PhotoAdapter(Activity context, List<PhotoDbModel> objects, PhotoListener photoListener) {
        models = objects;
        listener = photoListener;
        photoSize = (int) context.getResources().getDimension(R.dimen.photo_size_large);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Picasso.with(holder.itemView.getContext())
                .load(models.get(position).getUrl())
                .resize(photoSize, photoSize)
                .centerCrop()
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;

        public Holder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.ivPhoto);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPhotoClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface PhotoListener {
        void onPhotoClicked(int position);
    }
}
