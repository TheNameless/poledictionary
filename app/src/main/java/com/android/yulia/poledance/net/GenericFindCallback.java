package com.android.yulia.poledance.net;

import com.parse.ParseException;

import java.util.ArrayList;

/**
 * Created by thenameless on 11.01.16.
 */
public interface GenericFindCallback<T> {

    void find(ArrayList<T> models);

    void error(ParseException e);
}
