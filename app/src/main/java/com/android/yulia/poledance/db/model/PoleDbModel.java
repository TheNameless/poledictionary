package com.android.yulia.poledance.db.model;

import com.android.yulia.poledance.db.HelperFactory;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by thenameless on 11.01.16.
 */
@DatabaseTable(tableName = "poleTricks")
public class PoleDbModel {

    public final static String ELEMENT_NAME_FIELD = "name";
    public final static String ELEMENT_DESCRIPTION_FIELD = "description";
    public final static String ELEMENT_IS_FAVORITE_FIELD = "favorite";
    public final static String ELEMENT_IS_DONE_FIELD = "done";
    public final static String ELEMENT_VIDEOS_FIELD = "videos";
    public final static String ELEMENT_PHOTOS_FIELD = "photos";
    public final static String ELEMENT_PARSE_ID_FIELD = "parse_id";
    public final static String ELEMENT_LOCAL_ID_FIELD = "local_id";
    public final static String ELEMENT_POSITION_FIELD = "position";

    @DatabaseField(generatedId = true, columnName = ELEMENT_LOCAL_ID_FIELD)
    private int Id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = ELEMENT_NAME_FIELD)
    private String name;

    @DatabaseField(dataType = DataType.STRING, columnName = ELEMENT_DESCRIPTION_FIELD)
    private String description;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = ELEMENT_IS_FAVORITE_FIELD)
    private boolean favorite;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = ELEMENT_IS_DONE_FIELD)
    private boolean done;

    @DatabaseField(dataType = DataType.STRING, columnName = ELEMENT_PARSE_ID_FIELD)
    private String parseId;

    @DatabaseField(dataType = DataType.INTEGER, columnName = ELEMENT_POSITION_FIELD)
    private int position;

    @ForeignCollectionField(eager = true, columnName = ELEMENT_VIDEOS_FIELD)
    private Collection<VideoDbModel> videos = new ArrayList<>();

    @ForeignCollectionField(eager = true, columnName = ELEMENT_PHOTOS_FIELD)
    private Collection<PhotoDbModel> photos = new ArrayList<>();

    public void addVideo(VideoDbModel mediaDbModel) throws SQLException {
        mediaDbModel.setPoleDbModel(this);
//        HelperFactory.getHelper().getVideoDAO().create(mediaDbModel);
        videos.add(mediaDbModel);
    }

    public void addPhoto(PhotoDbModel mediaDbModel) throws SQLException {
        mediaDbModel.setPoleDbModel(this);
//        HelperFactory.getHelper().getPhotoDAO().create(mediaDbModel);
        photos.add(mediaDbModel);
    }

    public void removeMedia() throws SQLException {
        for (VideoDbModel mediaDbModel : videos) {
            HelperFactory.getHelper().getVideoDAO().delete(mediaDbModel);
        }

        for (PhotoDbModel mediaDbModel : photos) {
            HelperFactory.getHelper().getPhotoDAO().delete(mediaDbModel);
        }

        videos.clear();
        photos.clear();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Collection<VideoDbModel> getVideos() {
        return videos;
    }

    public void setVideos(Collection<VideoDbModel> videos) {
        this.videos = videos;
    }

    public Collection<PhotoDbModel> getPhotos() {
        return photos;
    }

    public void setPhotos(Collection<PhotoDbModel> photos) {
        this.photos = photos;
    }


    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getParseId() {
        return parseId;
    }

    public void setParseId(String parseId) {
        this.parseId = parseId;
    }

    public int getId() {
        return Id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public PoleDbModel(){
    }
}
