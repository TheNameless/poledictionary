package com.android.yulia.poledance.core;


import android.support.v4.app.FragmentManager;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.ui.fragment.DoneListFragment;
import com.android.yulia.poledance.ui.fragment.DoneListFragment_;
import com.android.yulia.poledance.ui.fragment.DrawerFragment;
import com.android.yulia.poledance.ui.fragment.DrawerFragment_;
import com.android.yulia.poledance.ui.fragment.ElementsListFragment;
import com.android.yulia.poledance.ui.fragment.ElementsListFragment_;
import com.android.yulia.poledance.ui.fragment.FavoritesListFragment;
import com.android.yulia.poledance.ui.fragment.FavoritesListFragment_;
import com.android.yulia.poledance.ui.fragment.SettingsFragment;
import com.android.yulia.poledance.ui.fragment.SettingsFragment_;


public class FragmentLauncher {

    private FragmentManager fragmentManager;

    public FragmentLauncher(FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    public void launchAllElementsFragment(){
        ElementsListFragment fragment = new ElementsListFragment_();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }


    public void launchFavoritesFragment() {
        FavoritesListFragment fragment = new FavoritesListFragment_();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    public void launchDoneFragment() {
        DoneListFragment fragment = new DoneListFragment_();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }


    public void launchSettingsFragment() {
        SettingsFragment fragment = new SettingsFragment_();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    public void launchDrawerFragment(){
        DrawerFragment drawerFragment = new DrawerFragment_();

        fragmentManager.beginTransaction()
                .add(R.id.drawer_fragment, drawerFragment).commit();
    }

}
