package com.android.yulia.poledance.db.model;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by thenameless on 11.01.16.
 */
@DatabaseTable(tableName = "photo")
public class PhotoDbModel extends MediaDbModel{

    public PhotoDbModel() {

    }

    public PhotoDbModel(String url) {
        setUrl(url);
    }
}
