package com.android.yulia.poledance.db.model;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by thenameless on 11.01.16.
 */
@DatabaseTable(tableName = "video")
public class VideoDbModel extends MediaDbModel{

    public VideoDbModel() {

    }

    public VideoDbModel(String url) {
        setUrl(url);
    }
}
