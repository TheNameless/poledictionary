package com.android.yulia.poledance.ui.views;

import android.widget.ImageView;

/**
 * Created by user on 24-02-16.
 */
public interface ElementClickListener {
    void onClick(int position, ImageView imageView);

    void setFavorite(int position);

    void setDone(int position);
}
