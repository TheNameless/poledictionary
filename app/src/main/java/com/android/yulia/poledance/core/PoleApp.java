package com.android.yulia.poledance.core;

import android.app.Application;

import com.parse.Parse;
import com.android.yulia.poledance.db.HelperFactory;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import org.androidannotations.annotations.EApplication;

/**
 * Created by thenameless on 11.01.16.
 */
@EApplication
public class PoleApp extends Application {

    private RefWatcher refWatcher;

    public RefWatcher getRefWatcher() {
        return refWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        refWatcher = LeakCanary.install(this);
        Parse.enableLocalDatastore(this);

        Parse.initialize(this);
        HelperFactory.setHelper(getApplicationContext());

    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
