package com.android.yulia.poledance.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.utils.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;

/**
 * Created by user on 26-01-16.
 */
public class PasteEditText extends AppCompatEditText {

    private PasteListener listener;

    public void setListener(PasteListener listener) {
        this.listener = listener;
    }

    public PasteEditText(Context context) {
        super(context);
        init();
    }

    public PasteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PasteEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_plus_dark, 0);

        setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == KeyEvent.KEYCODE_ENTER) {
                    performDone();
                    return true;
                }
                return false;
            }
        });
    }

    private void performDone() {
        final String text = getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            return;
        }

        Picasso.with(getContext()).load(text).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                listener.onPasteOrDone(text);
                Utils.hideKeyboard(PasteEditText.this);
                setText("");
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.d(PasteEditText.class.getCanonicalName(), "error");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Rect bounds;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int actionX = (int) event.getX();
            int actionY = (int) event.getY();

            Drawable drawableRight = getCompoundDrawables()[2];

            if (drawableRight != null) {

                bounds = drawableRight.getBounds();

                int x, y;
                int extraTapArea = 13;

                /**
                 * IF USER CLICKS JUST OUT SIDE THE RECTANGLE OF THE DRAWABLE
                 * THAN ADD X AND SUBTRACT THE Y WITH SOME VALUE SO THAT AFTER
                 * CALCULATING X AND Y CO-ORDINATE LIES INTO THE DRAWBABLE
                 * BOUND. - this process help to increase the tappable area of
                 * the rectangle.
                 */
                x = (int) (actionX + extraTapArea);
                y = (int) (actionY - extraTapArea);

                /**Since this is right drawable subtract the value of x from the width
                 * of view. so that width - tappedarea will result in x co-ordinate in drawable bound.
                 */
                x = getWidth() - x;

                 /*x can be negative if user taps at x co-ordinate just near the width.
                 * e.g views width = 300 and user taps 290. Then as per previous calculation
                 * 290 + 13 = 303. So subtract X from getWidth() will result in negative value.
                 * So to avoid this add the value previous added when x goes negative.
                 */

                if(x <= 0){
                    x += extraTapArea;
                }

                 /* If result after calculating for extra tappable area is negative.
                 * assign the original value so that after subtracting
                 * extratapping area value doesn't go into negative value.
                 */

                if (y <= 0)
                    y = actionY;

                /**If drawble bounds contains the x and y points then move ahead.*/
                if (bounds.contains(x, y) && listener != null) {
                    performDone();
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return false;
                }
                return super.onTouchEvent(event);
            }

        }
        return super.onTouchEvent(event);
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//        if (keyCode == KeyEvent.KEYCODE_ENTER) {
//            listener.onPasteOrDone(getText().toString().trim());
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public boolean onTextContextMenuItem(int id) {

        boolean consumed = super.onTextContextMenuItem(id);

        switch (id){
            case android.R.id.paste:
                onTextPaste();
                break;
        }
        return consumed;
    }

    /**
     * Text was pasted into the EditText.
     */
    public void onTextPaste(){
        performDone();
    }

    public interface PasteListener {
        void onPasteOrDone(String path);
    }
}
