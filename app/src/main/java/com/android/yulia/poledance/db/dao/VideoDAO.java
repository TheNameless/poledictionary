package com.android.yulia.poledance.db.dao;


import com.android.yulia.poledance.db.model.VideoDbModel;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by thenameless on 11.01.16.
 */
public class VideoDAO extends BaseDaoImpl<VideoDbModel, Integer> {

    public VideoDAO(ConnectionSource connectionSource,
                       Class<VideoDbModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}