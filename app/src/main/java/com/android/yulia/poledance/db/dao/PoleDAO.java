package com.android.yulia.poledance.db.dao;


import com.android.yulia.poledance.db.model.PoleDbModel;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by thenameless on 11.01.16.
 */
public class PoleDAO extends BaseDaoImpl<PoleDbModel, Integer> {

    public PoleDAO(ConnectionSource connectionSource,
                      Class<PoleDbModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<PoleDbModel> getAllPoleTricks() throws SQLException{
        QueryBuilder<PoleDbModel, Integer> queryBuilder = queryBuilder();
        queryBuilder.orderBy(PoleDbModel.ELEMENT_POSITION_FIELD, true);
        queryBuilder.orderBy(PoleDbModel.ELEMENT_LOCAL_ID_FIELD, false);
        PreparedQuery<PoleDbModel> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public PoleDbModel getElementByParseId(String parseId)  throws SQLException{
        QueryBuilder<PoleDbModel, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(PoleDbModel.ELEMENT_PARSE_ID_FIELD, parseId);
        PreparedQuery<PoleDbModel> preparedQuery = queryBuilder.prepare();
        List<PoleDbModel> goalList = query(preparedQuery);
        if (goalList == null || goalList.size() == 0) {
            return null;
        }
        return goalList.get(0);
    }

    public List<PoleDbModel> getFavorites()  throws SQLException{
        QueryBuilder<PoleDbModel, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(PoleDbModel.ELEMENT_IS_FAVORITE_FIELD, true);
        queryBuilder.orderBy(PoleDbModel.ELEMENT_POSITION_FIELD, false);
        queryBuilder.orderBy(PoleDbModel.ELEMENT_LOCAL_ID_FIELD, false);
        PreparedQuery<PoleDbModel> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<PoleDbModel> getDone()  throws SQLException{
        QueryBuilder<PoleDbModel, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(PoleDbModel.ELEMENT_IS_DONE_FIELD, true);
        PreparedQuery<PoleDbModel> preparedQuery = queryBuilder.prepare();
        queryBuilder.orderBy(PoleDbModel.ELEMENT_POSITION_FIELD, false);
        queryBuilder.orderBy(PoleDbModel.ELEMENT_LOCAL_ID_FIELD, false);
        return query(preparedQuery);
    }
}