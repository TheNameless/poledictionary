package com.android.yulia.poledance.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.net.ParseHelper;
import com.android.yulia.poledance.ui.adapter.PhotoAdapter;
import com.android.yulia.poledance.ui.adapter.PhotosAdapter;
import com.android.yulia.poledance.ui.adapter.VideoAdapter;
import com.android.yulia.poledance.ui.fragment.BaseFragment;
import com.android.yulia.poledance.ui.views.HorizontalSpaceItemDecoration;
import com.android.yulia.poledance.utils.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by user on 16-01-16.
 */

@EActivity(R.layout.activity_element_details)
public class ElementViewActivity extends BaseActivity implements PhotoAdapter.PhotoListener, VideoAdapter.VideoListener {

    private static final String ID_KEY = "id_key";
    private PoleDbModel poleDbModel;

    @ViewById(R.id.item_element_done) FloatingActionButton ivDone;
    @ViewById(R.id.item_element_favorite) FloatingActionButton ivFavorite;

    public static void start(BaseFragment fragment, String id, int position, ImageView imageView) {
        ElementViewActivity_.intent(fragment).extra(ID_KEY, id)
                .withOptions(Utils.getTransitionsBundle(fragment.getActivity(), imageView)).startForResult(position);
    }

    @AfterViews
    protected void onCreate() {
        setHomeIndicator();
        String id = getIntent().getStringExtra(ID_KEY);
        poleDbModel = DatabaseManager.getElementByParseId(id);

        if (poleDbModel == null) {
            finish();
            return;
        }

        setActionbarTitle(poleDbModel.getName());
        fillViews();
    }

    private void fillViews() {
        TextView tvDescription = (TextView) findViewById(R.id.item_element_description);
        final ImageView ivPhoto = (ImageView) findViewById(R.id.item_element_photo);

        if (!TextUtils.isEmpty(poleDbModel.getDescription())) {

            tvDescription.setText(poleDbModel.getDescription());
        } else {
            tvDescription.setText(R.string.text_no_description);
        }

        String previewUrl = Utils.DEFAULT_PHOTO;

        if (poleDbModel.getPhotos() != null && poleDbModel.getPhotos().size() > 0) {
            previewUrl = new ArrayList<>(poleDbModel.getPhotos()).get(0).getUrl();
        }

        Picasso.with(this).load(previewUrl).into(ivPhoto);

        Picasso.with(this).load(previewUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                initPalette(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        ivDone.setImageResource(poleDbModel.isDone() ? R.drawable.ic_done_on : R.drawable.ic_done_off);
        ivFavorite.setImageResource(poleDbModel.isFavorite() ? R.drawable.ic_star_on : R.drawable.ic_star_off);

        PhotoAdapter photoAdapter = new PhotoAdapter(this, new ArrayList<>(poleDbModel.getPhotos()), this);
        RecyclerView gridView = (RecyclerView) findViewById(R.id.gvPhotos);
        customizeMediaList(gridView);
        gridView.setAdapter(photoAdapter);

        VideoAdapter videoAdapter = new VideoAdapter(this, new ArrayList<>(poleDbModel.getVideos()), this);
        RecyclerView rvVideo = (RecyclerView) findViewById(R.id.gvVideos);
        customizeMediaList(rvVideo);
        rvVideo.setAdapter(videoAdapter);
    }

    private void customizeMediaList(RecyclerView rvMedia) {
        rvMedia.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvMedia.addItemDecoration(new HorizontalSpaceItemDecoration((int) getResources().getDimension(R.dimen.photo_divider)));
        rvMedia.setHasFixedSize(true);
    }

    @Click(R.id.item_element_done)
    protected void doneClicked() {
        changeDone();
    }

    @Click(R.id.item_element_favorite)
    protected void favoriteClicked() {
        changeFavorite();
    }

    private void changeFavorite() {
        ParseHelper.setFavorite(poleDbModel.getParseId());

        poleDbModel.setFavorite(!poleDbModel.isFavorite());
        DatabaseManager.updateModel(poleDbModel);
        ivFavorite.setImageResource(poleDbModel.isFavorite() ? R.drawable.ic_star_on : R.drawable.ic_star_off);
    }

    private void changeDone() {
        ParseHelper.setDone(poleDbModel.getParseId());

        poleDbModel.setDone(!poleDbModel.isDone());

        DatabaseManager.updateModel(poleDbModel);
        ivDone.setImageResource(poleDbModel.isDone() ? R.drawable.ic_done_on : R.drawable.ic_done_off);

    }

    @Override
    public void onPhotoClicked(int position) {
        PhotosActivity.start(this, poleDbModel.getParseId(), position);
    }

    @Override
    public void onVideoClicked(String url) {
        VideoActivity.start(this, url);
    }

    private void initPalette(Bitmap bitmap) {

        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
                toolbarLayout.setExpandedTitleColor(palette.getDarkMutedColor(Color.WHITE));
            }
        });
    }
}
