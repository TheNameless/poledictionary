package com.android.yulia.poledance.ui.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;

/**
 * Created by thenameless on 24.01.16.
 */
public class SwipeListener implements View.OnTouchListener {

    private static final int Y_THRESHOLD = 150;
    private static final int VELOCITY_THRESHOLD = 5000;
    private static final int ANIMATION_DURATION = 500;

    private int previousFingerPosition = 0;
    private int baseLayoutPosition = 0;
    private int defaultViewHeight;

    private boolean isClosing = false;
    private boolean isScrollingUp = false;
    private boolean isScrollingDown = false;
    private View base;
    private VelocityTracker velocityTracker;

    private SwipeCompleteListener listener;

    public SwipeListener(SwipeCompleteListener listener, View base) {
        this.listener = listener;
        this.base = base;
        velocityTracker = VelocityTracker.obtain();
    }

    public void closeUpAndDismissDialog(int currentPosition) {
        isClosing = true;
        playAnimation(ObjectAnimator.ofFloat(base, "y", currentPosition, -base.getHeight()));

    }

    public void closeDownAndDismissDialog(int currentPosition) {
        isClosing = true;
        Display display = ((Activity) base.getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenHeight = size.y;

        playAnimation(ObjectAnimator.ofFloat(base, "y", currentPosition, screenHeight + base.getHeight()));
    }

    private void playAnimation(ObjectAnimator yAnimation) {
        final AnimatorSet set1 = new AnimatorSet();
        set1.play(yAnimation);

        final AnimatorSet set2 = new AnimatorSet();
        set2.play(ObjectAnimator.ofFloat(base, View.ALPHA, base.getAlpha(), 0));

        final AnimatorSet set3 = new AnimatorSet();
        set3.playTogether(set1, set2);
        set3.setDuration(ANIMATION_DURATION);
        set3.addListener(animatorListener);
        set3.start();
    }

    private Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {
            listener.onSwipeComplete();
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }

    };


    @Override
    public boolean onTouch(View view, MotionEvent event) {

        if (event.getPointerCount() != 1) {
            return false;
        }
        velocityTracker.addMovement(event);
        // Get finger position on screen
        final int Y = (int) event.getRawY();

        // Switch on motion event type
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                // save default base layout height
                defaultViewHeight = base.getHeight();

                // Init finger and view position
                previousFingerPosition = Y;
                baseLayoutPosition = (int) base.getY();
                break;

            case MotionEvent.ACTION_UP:

                if (!isClosing) {
                    base.setAlpha(1);
                }
                // If user was doing a scroll up
                if (isScrollingUp) {
                    // Reset baselayout position
                    base.setY(0);
                    // We are not in scrolling up mode anymore
                    isScrollingUp = false;
                }

                // If user was doing a scroll down
                if (isScrollingDown) {
                    // Reset baselayout position
                    base.setY(0);
                    // Reset base layout size
                    base.getLayoutParams().height = defaultViewHeight;
                    base.requestLayout();
                    // We are not in scrolling down mode anymore
                    isScrollingDown = false;
                }

                break;
            case MotionEvent.ACTION_MOVE:

                if (!isScrollingDown && !isScrollingUp && Math.abs(Y - previousFingerPosition) < Y_THRESHOLD) {
                    return false;
                }

                if (!isClosing) {
                    int currentYPosition = (int) base.getY();

                    // If we scroll up
                    if (previousFingerPosition > Y) {
                        // First time android rise an event for "up" move
                        if (!isScrollingUp) {
                            isScrollingUp = true;
                        }

                        // Has user scroll down before -> view is smaller than it's default size -> resize it instead of change it position
                        if (base.getHeight() < defaultViewHeight) {
                            base.getLayoutParams().height = base.getHeight() - (Y - previousFingerPosition);
                            base.requestLayout();
                        } else {
                            // Has user scroll enough to "auto close" popup ?
                            velocityTracker.computeCurrentVelocity(1000);

                            if (Math.abs(velocityTracker.getYVelocity()) > VELOCITY_THRESHOLD || (baseLayoutPosition - currentYPosition) > defaultViewHeight / 3) {

                                closeUpAndDismissDialog(currentYPosition);
                                return true;
                            }

                            //
                        }
                        base.setY(base.getY() + (Y - previousFingerPosition));

                    }
                    // If we scroll down
                    else {

                        // First time android rise an event for "down" move
                        if (!isScrollingDown) {
                            isScrollingDown = true;
                        }

                        if (base.getHeight() < defaultViewHeight) {
                            base.getLayoutParams().height = base.getHeight() + (Y - previousFingerPosition);
                            base.requestLayout();
                        } else {
                            // Has user scroll enough to "auto close" popup ?
                            velocityTracker.computeCurrentVelocity(1000);

                            if (Math.abs(velocityTracker.getYVelocity()) > VELOCITY_THRESHOLD || Math.abs(baseLayoutPosition - currentYPosition) > defaultViewHeight / 3) {
                                closeDownAndDismissDialog(currentYPosition);
                                return true;
                            }
                        }

                        // Change base layout size and position (must change position because view anchor is top left corner)

                        base.setY(base.getY() + (Y - previousFingerPosition));
//                        base.getLayoutParams().height = base.getHeight() - (Y - previousFingerPosition);
//                        base.requestLayout();


                    }

                    base.setAlpha(1 - Math.abs(base.getY()) / (defaultViewHeight / 2));

                    base.requestLayout();
                    // Update position
                    previousFingerPosition = Y;
                }
                break;

        }
        velocityTracker.clear();
        return false;
    }

    public interface SwipeCompleteListener {
        void onSwipeComplete();
    }
}
