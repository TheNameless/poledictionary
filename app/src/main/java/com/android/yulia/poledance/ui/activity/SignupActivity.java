package com.android.yulia.poledance.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.SignUpCallback;
import com.android.yulia.poledance.R;
import com.android.yulia.poledance.net.ParseHelper;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

/**
 * Created by thenameless on 11.01.16.
 */

@EActivity(R.layout.activity_signup)
public class SignupActivity extends Activity {

    public static void start(Context context) {
        SignupActivity_.intent(context).start();
    }

    @Click(R.id.btn_signup)
    public void signupClicked() {
        signup();
    }

    private void signup() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.show();

        EditText etName = (EditText) findViewById(R.id.etUserName);
        EditText etPassword = (EditText) findViewById(R.id.etPassword);

        ParseHelper.signUp(etName.getText().toString().trim(), etPassword.getText().toString().trim(), new SignUpCallback() {

            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    MainActivity.start(SignupActivity.this, true);
                    finish();
                } else {
                    Toast.makeText(SignupActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
