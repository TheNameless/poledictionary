package com.android.yulia.poledance.ui.fragment;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.utils.Utils;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.drawer_fragment)
public class DrawerFragment extends BaseFragment {

    @ViewById(R.id.navigation_view) NavigationView navigationView;


    @AfterViews
    public void onViewCreated() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                selectItem(menuItem);
                return true;
            }
        });

        ImageView header = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.ivDrawerHeader);
        Picasso.with(getActivity())
                .load(Utils.DEFAULT_PHOTO)
//                .resize(size, size)
//                .centerCrop()
                .into(header);

        selectItem(navigationView.getMenu().findItem(R.id.drawer_all));
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(MenuItem menuItem) {
        activityBridge.onCloseDrawer();
        switch (menuItem.getItemId()) {
            case R.id.drawer_all:
                activityBridge.getFragmentLauncher().launchAllElementsFragment();
                break;
            case R.id.drawer_favorites:
                activityBridge.getFragmentLauncher().launchFavoritesFragment();
                break;
            case R.id.drawer_done:
                activityBridge.getFragmentLauncher().launchDoneFragment();
                break;

            case R.id.drawer_settings:
                activityBridge.getFragmentLauncher().launchSettingsFragment();
                break;

        }

        activityBridge.setActionbarTitle(menuItem.getTitle().toString());
    }
}
