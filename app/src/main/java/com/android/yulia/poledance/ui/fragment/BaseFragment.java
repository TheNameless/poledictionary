package com.android.yulia.poledance.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.core.ActivityBridge;
import com.android.yulia.poledance.core.PoleApp;
import com.squareup.leakcanary.RefWatcher;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EFragment;

/**
 * Created by thenameless on 11.01.16.
 */

public abstract class BaseFragment extends Fragment {


    protected ActivityBridge activityBridge;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBridge = (ActivityBridge) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return null;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ((PoleApp)getActivity().getApplication()).getRefWatcher();
        refWatcher.watch(this);
    }

}
