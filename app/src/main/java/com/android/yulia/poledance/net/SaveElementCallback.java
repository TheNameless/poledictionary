package com.android.yulia.poledance.net;

/**
 * Created by user on 27-01-16.
 */
public interface SaveElementCallback {

    void onSaved(String parseId);

    void error(String error);
}
