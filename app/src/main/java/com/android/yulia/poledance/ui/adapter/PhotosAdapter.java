package com.android.yulia.poledance.ui.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.android.yulia.poledance.ui.views.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user on 16-01-16.
 */
public class PhotosAdapter  extends PagerAdapter{

    private ArrayList<PhotoDbModel> models;

    private View.OnTouchListener touchListener;

    public PhotosAdapter(ArrayList<PhotoDbModel> models, View.OnTouchListener touchListener) {
        this.models = models;
        this.touchListener = touchListener;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.item_photo_pager, container, false);

        TouchImageView imageView = (TouchImageView) itemView.findViewById(R.id.imageView);
        imageView.setOnTouchListener(touchListener);
        Picasso.with(imageView.getContext())
                .load(models.get(position).getUrl())
                .into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}
