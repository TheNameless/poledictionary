package com.android.yulia.poledance.net.model;

import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by thenameless on 11.01.16.
 */
public class ElementModel {

    private String name;
    private String description;
    private List<String> videos;
    private List<String> photos;
    private boolean isDone;
    private boolean isFavorite;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getVideos() {
        return videos;
    }

    public void setVideos(List<String> videos) {
        this.videos = videos;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public ElementModel() {
    }

    public ElementModel(ParseObject parseObject) {
        this.name = parseObject.getString("name");
        this.description = parseObject.getString("description");

        this.id = parseObject.getObjectId();

        List<String> favorites = parseObject.getList("favorites");
        List<String> done = parseObject.getList("done");

        if (favorites != null) {
            this.isFavorite = favorites.contains(ParseUser.getCurrentUser().getObjectId());
        }
        if (done != null) {
            this.isDone = done.contains(ParseUser.getCurrentUser().getObjectId());
        }
        this.videos = parseObject.getList("videos");
        this.photos = parseObject.getList("photos");
    }
}
