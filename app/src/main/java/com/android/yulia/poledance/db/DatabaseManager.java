package com.android.yulia.poledance.db;

import com.android.yulia.poledance.db.dao.PoleDAO;
import com.android.yulia.poledance.net.model.ElementModel;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.db.model.VideoDbModel;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by user on 16-01-16.
 */
public class DatabaseManager {

    public static void updateModel(PoleDbModel model) {
        try {
            HelperFactory.getHelper().getPoleDAO().update(model);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void refreshModel(PoleDbModel model) {
        try {
            HelperFactory.getHelper().getPoleDAO().refresh(model);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<PoleDbModel> getFavorites() {
        ArrayList<PoleDbModel> poleDbModels = new ArrayList<>();
        try {
            poleDbModels.addAll(HelperFactory.getHelper().getPoleDAO().getFavorites());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return poleDbModels;
    }

    public static ArrayList<PoleDbModel> getDone() {
        ArrayList<PoleDbModel> poleDbModels = new ArrayList<>();
        try {
            poleDbModels.addAll(HelperFactory.getHelper().getPoleDAO().getDone());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return poleDbModels;
    }

    public static ArrayList<PoleDbModel> getAllElements() {
        ArrayList<PoleDbModel> poleDbModels = new ArrayList<>();
        try {
            poleDbModels.addAll(HelperFactory.getHelper().getPoleDAO().getAllPoleTricks());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return poleDbModels;
    }

    public static void updateDB(ArrayList<ElementModel> models) throws SQLException {
        for (ElementModel elementModel : models) {
            insertOrUpdateModel(elementModel);

        }
    }

    public static void insertOrUpdateModel(ElementModel elementModel) throws SQLException {
        PoleDbModel poleDbModel = new PoleDbModel();
        poleDbModel.setName(elementModel.getName());
        poleDbModel.setDescription(elementModel.getDescription());
        poleDbModel.setDone(elementModel.isDone());
        poleDbModel.setFavorite(elementModel.isFavorite());

        poleDbModel.setParseId(elementModel.getId());

        PoleDbModel element = HelperFactory.getHelper().getPoleDAO().getElementByParseId(elementModel.getId());
        if (element != null) {
            HelperFactory.getHelper().getPoleDAO().update(poleDbModel);
        } else {
            HelperFactory.getHelper().getPoleDAO().create(poleDbModel);
        }

        PoleDbModel resultModel = HelperFactory.getHelper().getPoleDAO().getElementByParseId(elementModel.getId());
        resultModel.removeMedia();
        if (elementModel.getVideos() != null) {
            for (String video : elementModel.getVideos()) {
                VideoDbModel videoDbModel = new VideoDbModel(video);
                resultModel.addVideo(videoDbModel);
            }
        }

        if (elementModel.getPhotos() != null) {
            for (String photo : elementModel.getPhotos()) {
                PhotoDbModel photoDbModel = new PhotoDbModel(photo);
                resultModel.addPhoto(photoDbModel);
            }
        }

        HelperFactory.getHelper().getPoleDAO().update(resultModel);
    }

    public static PoleDbModel getElementByParseId(String parseId) {
        try {
            return HelperFactory.getHelper().getPoleDAO().getElementByParseId(parseId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  null;
    }
}
