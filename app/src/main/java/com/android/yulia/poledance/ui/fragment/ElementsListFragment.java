package com.android.yulia.poledance.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.db.HelperFactory;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.net.ParseHelper;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.EFragment;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by thenameless on 11.01.16.
 */
@EFragment(R.layout.fragment_elements_list)
public class ElementsListFragment extends BaseElementsFragment {

    @Override
    protected boolean enableDragging() {
        return true;
    }

    @Override
    protected ArrayList<PoleDbModel> getElements() {
        return DatabaseManager.getAllElements();
    }


    @Override
    public void setFavorite(int position) {

        PoleDbModel model = searchModels.get(position);
        ParseHelper.setFavorite(model.getParseId());

        model.setFavorite(!model.isFavorite());
        DatabaseManager.updateModel(model);

        adapter.notifyItemChanged(position);
    }

    @Override
    public void setDone(int position) {
        PoleDbModel model = searchModels.get(position);
        ParseHelper.setDone(model.getParseId());

        model.setDone(!model.isDone());

        DatabaseManager.updateModel(model);
        adapter.notifyItemChanged(position);
    }

    @Subscribe
    public void answerAvailable(String code) {

        fillList();
        Toast.makeText(getActivity(), "updated", Toast.LENGTH_SHORT).show();
    }

}
