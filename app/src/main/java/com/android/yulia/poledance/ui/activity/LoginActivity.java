package com.android.yulia.poledance.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.android.yulia.poledance.R;
import com.android.yulia.poledance.net.ParseHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

/**
 * Created by thenameless on 11.01.16.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends Activity {

    public static void start(Context context) {
        LoginActivity_.intent(context).flags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP).start();
    }

    @AfterViews
    protected void onCreate() {
        ParseUser parseUser = ParseUser.getCurrentUser();

        if (parseUser != null) {
            MainActivity.start(LoginActivity.this);
            finish();
        }
    }

    @Click(R.id.btn_login)
    public void loginClicked() {
        login();
    }

    @Click(R.id.btn_signup)
    public void signupClicked() {
        SignupActivity.start(this);
    }


    private void login() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.show();

        EditText etName = (EditText) findViewById(R.id.etUserName);
        EditText etPassword = (EditText) findViewById(R.id.etPassword);

        ParseHelper.login(etName.getText().toString().trim(), etPassword.getText().toString().trim(), new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    MainActivity.start(LoginActivity.this, true);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
