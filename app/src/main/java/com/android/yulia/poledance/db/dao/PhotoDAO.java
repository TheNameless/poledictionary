package com.android.yulia.poledance.db.dao;


import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by thenameless on 11.01.16.
 */
public class PhotoDAO extends BaseDaoImpl<PhotoDbModel, Integer> {

    public PhotoDAO(ConnectionSource connectionSource,
                       Class<PhotoDbModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}