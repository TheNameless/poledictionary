package com.android.yulia.poledance.ui.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.DatabaseManager;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.android.yulia.poledance.net.ParseHelper;
import com.android.yulia.poledance.net.SaveElementCallback;
import com.android.yulia.poledance.ui.adapter.PhotoAdapter;
import com.android.yulia.poledance.ui.fragment.BaseFragment;
import com.android.yulia.poledance.ui.views.HorizontalSpaceItemDecoration;
import com.android.yulia.poledance.ui.views.PasteEditText;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by user on 26-01-16.
 */
@EActivity(R.layout.activity_new_element)
public class CreateElementActivity extends BaseActivity implements PasteEditText.PasteListener, PhotoAdapter.PhotoListener {

    private ArrayList<PhotoDbModel> photos = new ArrayList<>();
    private PhotoAdapter photoAdapter;

    @ViewById(R.id.new_element_photo_preview) ImageView photoPreview;
    @ViewById(R.id.gvPhotos) RecyclerView gridView;
    @ViewById(R.id.newElementName) EditText etName;
    @ViewById(R.id.newElementDescription) EditText etDescription;

    public static final String PARSE_ID_KEY = "parse_id_key";

    public static void start(BaseFragment fragment, int code) {
        CreateElementActivity_.intent(fragment).startForResult(code);
    }

    @AfterViews
    protected void onCreate() {
        setHomeIndicator();

        final PasteEditText etPhoto = (PasteEditText) findViewById(R.id.newElementAddPhoto);
        etPhoto.setListener(this);

        photoAdapter = new PhotoAdapter(this, photos, this);
        gridView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        gridView.addItemDecoration(new HorizontalSpaceItemDecoration((int) getResources().getDimension(R.dimen.photo_divider)));
        gridView.setHasFixedSize(true);
        gridView.setAdapter(photoAdapter);
    }

    @Override
    public void onPasteOrDone(String path) {

        updateImage(path);
        photos.add(0, new PhotoDbModel(path));
        photoAdapter.notifyItemInserted(0);
        gridView.scrollToPosition(0);
    }

    @Override
    public void onPhotoClicked(int position) {
        updateImage(photos.get(position).getUrl());
    }

    private void updateImage(String path) {
        Picasso.with(this).load(path)
                .centerInside()
                .resize(photoPreview.getWidth(), photoPreview.getHeight())
                .into(photoPreview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                save();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Saving");
        dialog.setCancelable(false);
        dialog.show();
        String name = etName.getText().toString().trim();
        String description = etDescription.getText().toString().trim();

        ParseHelper.createElement(name, description, photos, false, false, new SaveElementCallback() {
            @Override
            public void onSaved(String parseId) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.putExtra(PARSE_ID_KEY, parseId);
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void error(String s) {
                dialog.dismiss();

                Toast.makeText(CreateElementActivity.this, s,  Toast.LENGTH_SHORT).show();
            }
        });
    }
}
