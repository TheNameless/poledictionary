package com.android.yulia.poledance.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.yulia.poledance.db.dao.PhotoDAO;
import com.android.yulia.poledance.db.dao.PoleDAO;
import com.android.yulia.poledance.db.dao.VideoDAO;
import com.android.yulia.poledance.db.model.PhotoDbModel;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.db.model.VideoDbModel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by thenameless on 11.01.16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final String DATABASE_NAME = "myappname.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DATABASE_VERSION = 1;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private PoleDAO poleDao = null;
    private VideoDAO videoDAO = null;
    private PhotoDAO photoDAO = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, PhotoDbModel.class);
            TableUtils.createTable(connectionSource, VideoDbModel.class);
            TableUtils.createTable(connectionSource, PoleDbModel.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    //Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        try {
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable(connectionSource, PoleDbModel.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }

    public PoleDAO getPoleDAO() throws SQLException {
        if (poleDao == null) {
            poleDao = new PoleDAO(getConnectionSource(), PoleDbModel.class);
        }
        return poleDao;
    }

    public VideoDAO getVideoDAO() throws SQLException {
        if (videoDAO == null) {
            videoDAO = new VideoDAO(getConnectionSource(), VideoDbModel.class);
        }
        return videoDAO;
    }

    public PhotoDAO getPhotoDAO() throws SQLException {
        if (photoDAO == null) {
            photoDAO = new PhotoDAO(getConnectionSource(), PhotoDbModel.class);
        }
        return photoDAO;
    }

    //выполняется при закрытии приложения
    @Override
    public void close() {
        super.close();
        poleDao = null;
        videoDAO = null;
        photoDAO = null;
    }
}
