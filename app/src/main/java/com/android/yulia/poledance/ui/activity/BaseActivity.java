package com.android.yulia.poledance.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.core.ActivityBridge;
import com.android.yulia.poledance.core.FragmentLauncher;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by thenameless on 12.01.16.
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity implements ActivityBridge {

    private FragmentLauncher fragmentLauncher;
    private Bus bus;

    @Nullable
    @ViewById(R.id.toolbar) Toolbar toolbar;

    @AfterViews
    void afterViews() {
        bus = new Bus(ThreadEnforcer.MAIN);
        fragmentLauncher = new FragmentLauncher(getSupportFragmentManager());

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    @Override
    public FragmentLauncher getFragmentLauncher() {
        return fragmentLauncher;
    }

    @Override
    public void onCloseDrawer() {

    }

    @Override
    public MaterialSearchView getSearchView() {
        return null;
    }

    @Override
    public void setActionbarTitle(int res) {
        setActionbarTitle(getString(res));
    }

    @Override
    public void setActionbarTitle(String title) {
        setTitle(title);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public Bus getBus() {
        return bus;
    }

    @Override
    public void setHomeIndicator() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
