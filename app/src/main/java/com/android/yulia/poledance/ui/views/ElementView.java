package com.android.yulia.poledance.ui.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.yulia.poledance.R;
import com.android.yulia.poledance.db.model.PoleDbModel;
import com.android.yulia.poledance.ui.adapter.ElementsAdapter;
import com.android.yulia.poledance.utils.Utils;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.DimensionRes;

import java.util.ArrayList;

/**
 * Created by user on 24-02-16.
 */

@EViewGroup(R.layout.item_element)
public class ElementView extends CardView {

    private ElementClickListener listener;
    private int position;

    @ViewById(R.id.item_element_name) TextView elementName;
    @ViewById(R.id.item_element_description) TextView elementDescription;
    @ViewById(R.id.item_element_photo) ImageView ivPhoto;
    @ViewById(R.id.item_element_favorite) ImageView ivFavorite;
    @ViewById(R.id.item_element_done) ImageView ivDone;
    @DimensionRes(R.dimen.photo_size) float size;

    private ElementsAdapter.DragListener dragListener;

    public ElementView(Context context, ElementClickListener listener) {
        super(context);
        init(listener);
    }

    private void init(ElementClickListener listener) {
        setUseCompatPadding(true);
        this.listener = listener;
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    @Click(R.id.ripple)
    protected void itemClicked() {
        listener.onClick(position, ivPhoto);
    }

    @Click(R.id.item_element_done)
    protected void doneClicked() {
        listener.setDone(position);
    }

    @Click(R.id.item_element_favorite)
    protected void favoriteClicked() {
        listener.setFavorite(position);
    }

    @LongClick(R.id.item_element_photo)
    protected void photoClicked() {
        dragListener.onDragStarted();
    }

    public void bind(PoleDbModel poleDbModel, int position) {

        this.position = position;
        elementName.setText(poleDbModel.getName());
        elementDescription.setText(poleDbModel.getDescription());
        ivDone.setBackgroundResource(poleDbModel.isDone() ? R.drawable.ic_done_on : R.drawable.ic_done_off);
        ivFavorite.setBackgroundResource(poleDbModel.isFavorite() ? R.drawable.ic_star_on : R.drawable.ic_star_off);

        String previewUrl = Utils.DEFAULT_PHOTO;

        if (poleDbModel.getPhotos() != null && poleDbModel.getPhotos().size() > 0) {
            previewUrl = new ArrayList<>(poleDbModel.getPhotos()).get(0).getUrl();
        }

        Picasso.with(getContext())
                .load(previewUrl)
                .resize((int)size, (int) size)
                .centerCrop()
                .into(ivPhoto);
    }

    public void setDragListener(ElementsAdapter.DragListener dragListener) {
        this.dragListener = dragListener;
    }
}
